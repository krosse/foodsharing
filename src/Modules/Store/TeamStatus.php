<?php

namespace Foodsharing\Modules\Store;

class TeamStatus
{
    final public const NoMember = 0;
    final public const Applied = 1;
    final public const WaitingList = 2;
    final public const Member = 3;
    final public const Coordinator = 4;
}
