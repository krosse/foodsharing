## 🌍 Overview

This is the code that powers
[foodsharing.de](https://foodsharing.de),
[foodsharing.at](https://foodsharing.at), and
[foodsharingschweiz.ch](https://foodsharingschweiz.ch).

## 💻 Development

Visit our [DevDocs](https://devdocs.foodsharing.network) to get started with development.

### Quickstart

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/foodsharing-dev/foodsharing/-/tree/master/)

Gitpod is a fast development environment in your browser.

## 💒 Community

Our developers hang out in the
[yunity slack](https://slackin.yunity.org) group in the
**#foodsharing-dev** and **#foodsharing-beta** channels. Come and say hi!

## 🔣 Translations

Translations are done via
[weblate](https://hosted.weblate.org/projects/foodsharing/). If you can help translate the app into a
new language, feel free to help!

## Testing
This project is tested with BrowserStack. Thank you!

## 👪 Contributors

We are incredibly thankful for every contributor who has helped make this project better. Your contributions, whether big or small, have made a significant impact on the success of foodsharing. Without your dedication and support, this project would not be where it is today.

Thank you for your time, effort, and expertise. We appreciate your contributions and look forward to continuing to work together to create a positive impact on the world.

This list is not exhaustive, but you can find a comprehensive list of code contributors on [GitLab](https://gitlab.com/foodsharing-dev/foodsharing/-/graphs/master).

Keep up the great work!
